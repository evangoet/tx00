-objectif
-données entrées/sorties
-dimension spatiale :cb de capteurs, localisation -> déterminer un endroit ou prédire le traffic

#Etude préalable : Prévision du traffic New Yorkais

Le sujet de l'étude que je vais réaliser pour cette TX est : "Prédiction du traffic New Yorkais."
Avant de se lancer dans ce projet, il est important de réaliser une étude préalable permettant d'affiner le sujet et de bien le délimiter.

## Objectif

De nombreuses villes se mettent à collecter des données sur leurs territoires. Ces données sont ensuite laisser à la disposition du grand public. L'objectif de cette TX est de s'appuyer sur ces données pour d'abord réussir à prédire l'afflux de véhicule sur une rue en fonction de l'heure et du jour de la semaine. Ensuite, prédire le nombre de véhicule pour un itinéraire prédéfini, qui rassemble plusieurs tronçon de rue et de proposer le meilleur itinéraire possible (voir si possible).

Discuter avec Mr Morel : Prédire des pattern en fonction du mois/jour/heure ou prédiction dans les 15 prochaines minutes. 
https://github.com/fabmob/trafic

#Itinéraire

## Données et capteurs 
### Description des données

### localisation des données


